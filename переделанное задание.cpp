﻿#pragma once
#include "stdafx.h"
#include "math.h"
#include "Form1.h"

// точка на плоскости
struct Point2D
{
	double x;//координата по X
	double y;//координата по Y
};

int fact1(int mmm, int xm[25], int ym[25])
{
	int i;
	System::Windows::Forms::PaintEventArgs^ e;
	for (i = 0;i<mmm;i++)
	{
		e->Graphics->DrawLine(System::Drawing::Pens::Green, xm[i], ym[i], xm[i + 1], ym[i + 1]);
	}
	return mmm + i;
}

int fact(int xm[25], int ym[25])
{
	// Для демонстрации
	int nmbmax; //максимально - допустимое число точек ломаной
	int	nmbp; //число вершин многоугольника, задающего район
	int nn; 
	Point2D z[25]; // Координаты точек
	Point2D p[25]; //координаты вершин многоугольника, задающего район
	double alf; // максимальный угол поворота
	double rmin; //минимальное расстояние между точками поворота
	double rmax; //максимально-допустимая длина ломаной
	double Pi;
	Pi = atan(1.) * 4;

	z[0] = { 4.5 , 5.0 };
	z[1] = Point2D(-1, 0);
	z[2] = Point2D(0, 0);
	z[3] = Point2D(0, 0);
	z[4] = Point2D(2, 2);
	z[5] = Point2D(0.8, -0.6);

	alf = Pi / 3;
	rmin = 1;
	rmax = 100;
	nmbmax = 25;
	nmbp = 4;

	p[0] = Point2D(0, 0);
	p[1] = Point2D(10, 0);
	p[2] = Point2D(10,10);
	p[3] = Point2D(0, 10);

	nn = EndSelect(z, p, nmbmax, nmbp, rmin, rmax, alf); 

	for (int k = 0;k<nn;k++)
	{
		xm[k] = floor(z[k].x * 150);
		ym[k] = floor(900 - z[k].y * 150);
	}
	return nn;
}

/*
Функция предназначена для определения лучшего варианта построения касательных для определения точек
поворота между началом входного и концом конечного векторов при условии:
 - угол поворота < alf;
 - длина отрезка между точками поворота > rmin.
Результат -число  точек поворота в лучшем варианте началом входного и концом конечного векторов

Формальные  аргументы.
 При входе :
   z[0] - координаты начала входного вектора
   z[1] - предпочтительное направление входного вектора
   z[2] - минимальное значение дипазона допустимых значений направления входного вектора
   z[3] - максимальное значение дипазона допустимых значений направления входного вектора
   z[4] - координаты конца конечергр вектора
   z[5] - направление конечного вектора
 alf - максимальный угол поворота
 rmin - минимальное расстояние между точками поворота
 rmax -максимально-допустимая длина ломаной
 nmbmax -максимально-допустимое число точек ломаной
 p[i] -координаты вершин многоугольника, задающего район
 nmbp -число вершин многоугольника, задающего район
 При выходе
	z[i] - координаты точек поворота лучшего варианта
  (точки нумеруются начиная с начала ломаной до ее конца).
 При этом :
   z[0] - координаты начала входного вектора
   z[nmb-1]-координаты конца конечного вектора
 где nmb - общее число точек ломаной, включая точки начала и конца.
 */
int EndSelect(Point2D zab[25], Point2D p[25], int nmbmax, int nmbp, double rmin, double rmax, double alf)
{
	Point2D zxy[25];
   double rlen,rl[5];
   int i,knmb,k,inds,indreg,indr,nmb,indloop,indvar,ipar[4];
   int iex[5],ireg[5],ir[5],inmb[5],iloop[5],nn,nk;
   for (i=1;i<5;i++)
   {
       for (k=0;k<7;k++) 
	   {
		   zxy[k] = zab[k];
       }

       indvar=i;
       nn=EndMarsh(zxy, rmin,rmax,rlen,alf,indvar,ipar,nmbmax);
       inds=ipar[0];
       nmb=ipar[1];
       indloop=ipar[2];
       indr=ipar[3];
       nk=RegBoundary(zxy, p, nmb, nmbp, indreg);

       // сохранение inds,indreg,indr,nmb,indloop,rlen
       iex[i]=inds;
       ireg[i]=indreg;
       ir[i]=indr;
       inmb[i]=nmb;
       iloop[i]=indloop;
       rl[i]=rlen;
  }

   // выбор допустимых вариантов по nmb с учетом inds,indreg,indr
  for (i=1;i<5;i++)
  {
      if(iex[i]==0)
      {
          if(ireg[i]>0) iex[i]=0;
          if(ir[i]>0)  iex[i]=1;
      }
  }

   // наилучший путь по nmb, при этом учет: indloop, rlen
   nmb=25;
   knmb=-1;
   for (i=1;i<5;i++)
     {
	    if(iex[i]==0)
	    {	
            if(inmb[i]<nmb)
		    {
                nmb=inmb[i];
                knmb=i;
		    }
        }
     } 
   if(knmb>0)
   {
       indloop=iloop[knmb];
       rlen=rl[knmb];
   }

   // Итоговый вариант
   for (k = 0;k<7;k++) {
	   zxy[k] = zab[k];
   }
   indvar=knmb;
   nn=EndMarsh(zxy,rmin,rmax,rlen,alf,indvar,ipar,nmbmax);
   nmb=ipar[1];

   for (k=0;k<nmb;k++)
   {
	   zab[k] = zxy[k];
   }
   return nmb;
}



/*
Функция предназначена для определения точек поворота между началом входного и концом конечного векторов при условии:
 - угол поворота < alf;
 - длина отрезка между точками поворота > rmin.
 Результат -число  точек поворота между началом входного и концом конечного векторов

Формальные аргументы.
 При входе :
   zxy[0] - координаты начала входного вектора
   zxy[1] - предпочтительное направление входного вектора
   zxy[2] - минимальное значение дипазона допустимых значений направления входного вектора
   zxy[3] - максимальное значение дипазона допустимых значений направления входного вектора
   zxy[4] - координаты конца конечергр вектора
   zxy[5] - направление конечного вектора
 alf - максимальный угол поворота
 rmin - минимальное расстояние между точками поворота
 rmax -максимально-допустимая длина ломаной
 indvar - номер варианта касательных
 nmbmax -максимально-допустимое число точек ломаной
 
 При выходе
	zxy[i] - координаты точки поворота с номером i
  (точки нумеруются начиная с начала ломаной до ее конца).
 При этом :
   zxy[0]  - координаты начала входного вектора
   zxy[nmb-1] -координаты конца конечного вектора
 где nmb - общее число точек ломаной, включая точки начала и конца.
 rlen - длина ломаной
 ipar -
 ipar[0] - признак существования решения
 ipar[1] - общее число точек ломаной
 ipar[2] - признак наличия петли
 ipar[3] - признак наличия отрезка с длиной меньшей,чем rmin
 */
int EndMarsh(Point2D zxy[25],double rmin,double rmax,double rlen,double alf, int indvar,int ipar[4],int nmbmax)
{
   Point2D kn, kf, on, of;
   Point2D n1, n2, f1, f2;
   double r,rr,d,d1,fin,fout,Pi,beta,gamm;
   int inds,nmb,indloop,indr;
   int nmb1,nmb2,i;
   Pi=atan(1.)*4;

   n1 = zxy[0];

   // конец входного вектора 
   fin=myAngleP(zxy[1]);
   n2 = Point2D(n1.x + rmin*cos(fin), n1.y + rmin*sin(fin));

   //начало финального вектора
   f2 = zxy[4];

   fout=myAngleP(zxy[5]);

   f1 = Point2D(f2.x - rmin*cos(fout), f2.y - rmin*sin(fout));
   for (i=0;i<25;i++)
   {
	   zxy[i] = Point2D(0, 0);
   }

   //определение центров окужностей
   r=rmin/(2*sin(alf/2));
   if((indvar==1)||(indvar==2))
   {
	  gamm=fin+Pi*0.5+alf/2;
   }
   else
   {
	  gamm=fin-Pi*0.5-alf/2;
   }

   on = Point2D(n2.x + r*cos(gamm), n2.y + r*sin(gamm));

   if((indvar==1)||(indvar==3))
   {
	  gamm=fin+Pi*0.5+alf/2;
   }
   else
   {
	  gamm=fin-Pi*0.5-alf/2;
   }

   of = Point2D(f2.x + r*cos(gamm), f2.y + r*sin(gamm));


   //определение существования решений
   if((indvar==1)||(indvar==4))
   {
      inds=0;
   }
   else
   {
	  d=myMod(on, of);
	  if((d-2*r)<0) inds=1;
	  else inds=0;
   }

   //определение точек касания
   if(inds==0)
   {
      gamm=myAngle(of, on);
      if(indvar==1)
	  {
         d=-Pi*0.5+gamm;
	     d1=d;
      }
      if(indvar==4)
	  {
         d=Pi*0.5+gamm;
	     d1=d;
      }
      if(indvar==2)
	  {
         rr=sqrt((of.x-on.x)*(of.x-on.x)+(of.y-on.y)*(of.y-on.y));
	     beta=asin((2*r)/rr);
         d=gamm+beta-Pi*0.5;
	     d1=gamm+beta+Pi*0.5;
      }
	  if (indvar == 1)
	  {
		  rr = sqrt((of.x - on.x)*(of.x - on.x) + (of.y - on.y)*(of.y - on.y));
		  beta = asin((2 * r) / rr);
		  d = gamm + beta + Pi*0.5;
		  d1 = gamm + beta - Pi*0.5;
	  }

	  kn = Point2D(on.x + r*cos(d), on.y + r*sin(d));
	  kf = Point2D(of.x + r*cos(d1), of.y + r*sin(d1));

      //определение точек поворота  
      gamm=myArg(kn,kf);
      if((indvar==1)||(indvar==2))
      {
	      d=gamm-fin;
	      d1=alf;
      }
      else
      {
	      d=fin-gamm;
 	      d1=-alf;
      }
      if(d>2*Pi) d=d-2*Pi;
      if(d<0) d=d+2*Pi;
      nmb1=d/alf;
	zxy[0] = n1;
	zxy[1] = n2;

      if(nmb1>0)
      {
          for (i=1;i<nmb1+1;i++)
          {
              d=i*d1+fin;
			  zxy[i + 1] = Point2D(zxy[i].x + rmin*cos(d), zxy[i].y + rmin*sin(d));
          }
      }
      if((indvar==1)||(indvar==3))
      {
	      d=fout-gamm;
	      d1=alf;
      }
      else
      {
	      d=gamm-fout;
	      d1=-alf;
      }
      if(d>2*Pi) d=d-2*Pi;
      if(d<0) d=d+2*Pi;
      nmb2=d/alf;
      zxy[nmb1+nmb2+2]=f1;
      zxy[nmb1+nmb2+3]=f2;
      if(nmb2>0)
      {
          for (i=1;i<nmb2+1;i++)
          {
              d=-i*d1+fout-Pi;
			  zxy[nmb1 + nmb2 + 2 - i] = Point2D(zxy[nmb1 + nmb2 + 3 - i].x + rmin*cos(d), zxy[nmb1 + nmb2 + 3 - i].y + rmin*sin(d));
          }
      }
//
      nmb=nmb1+nmb2+4;
      d=myMod(zxy[nmb1+1],zxy[nmb1+2]);
      if(d<rmin)
      {
	      indr=1;
      }
      if((nmb1+1)>(2*Pi/alf))
      {
	      indloop=1;
      }
      if((nmb2+1)>(2*Pi/alf))
      {
	      indloop=1;
      }
      rlen=0;
      for (i=1;i<nmb;i++)
      {
          d=zxy[i].y-zxy[i-1].y;
          d1=zxy[i].x-zxy[i-1].x;
          rr=sqrt(d*d+d1*d1);
          rlen=rlen+rr;
      }
//==================
      ipar[0]=inds;
      ipar[1]=nmb;
      ipar[2]=indloop;
      ipar[3]=indr;
   }
   return nmb;
}

int RegBoundary(Point2D zxy[25],Point2D p[25],int nmb,int nmbp,int indreg)
{
   int i,k;
   double d1,d2,Pi;
   Pi=atan(1.)*4;
   indreg=0;
   for (i=0;i<nmb;i++)
   {
      for (k=0;i<nmbp-1;i++)
      {
         d1=myArg(zxy[i],p[k]);
         d2=myArg(zxy[i],p[k+1]);
         d2=d2-d1;
         if(d2<0) d2=d2+2*Pi;
         if(d2>=Pi)indreg=indreg+1;
      }
     d1=myArg(zxy[i],p[nmbp-1]);
     d2=myArg(zxy[i],p[0]);
     d2=d2-d1;
     if(d2<0)d2=d2+2*Pi;
     if(d2>=Pi)indreg=indreg+1;
   }
   return indreg;
}

double myArg(Point2D n1,Point2D f1)
{
   double  d,dd,ddd,pi;
   pi=atan(1.)*4;
   d=f1.y-n1.y;
   dd=f1.x-n1.x;
   ddd=sqrt(d*d+dd*dd);
   d=d/ddd;
   dd=dd/ddd;
   if((d>=0)&&(dd>=0)) ddd=asin(d);
   if((d>=0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
   return ddd;
}


//модуль вектора (расстояние между двумя точками) (nx,ny)-начало,(fx,fy)-конец)
double myMod(Point2D n1, Point2D f1)
{
   double d,dd,ddd;
   d=f1.y-n1.y;
   dd=f1.x-n1.x;
   ddd=sqrt(d*d+dd*dd);
   return ddd;
}

double myAngle(Point2D n1, Point2D f1)
{
   double d,dd,ddd,pi;
   pi=atan(1.)*4;
   d=n1.y-f1.y;
   dd=n1.x-f1.x;
   ddd=sqrt(d*d+dd*dd);
   d=d/ddd;
   dd=dd/ddd;
   if((d>=0)&&(dd>=0)) ddd=asin(d);
   if((d>=0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
   return ddd;
}

double myAngleP(Point2D n1)
{
   double d,dd,ddd,pi;
   pi=atan(1.)*4;
   d=n1.y;
   dd=n1.x;
   ddd=sqrt(d*d+dd*dd);
   d=d/ddd;
   dd=dd/ddd;
   if((d>=0)&&(dd>=0)) ddd=asin(d);
   if((d>=0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
   return ddd;
}