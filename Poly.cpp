﻿#pragma once
#include "stdafx.h"
#include "math.h"
#include "Form1.h"

// точка на плоскости
struct Point2D
{
	double x;//координата по X
	double y;//координата по Y
};
// многоугольник на плоскости 
struct Polyline
{
	int vertex_count; //число вершин в многоугольнике 
	Point2D* vertex; //храение многоугольника, как массив вершин 
};

struct Vector
{
	Point2D begin; //координаты начала вектора
	Point2D end; //предпочтительное направление вектора
};

//параметры для работы алгоритма
struct Parameters
{
	Vector initialVec; //начальный вектор
	Vector finalVec;  //конечный вектор
	double rmin;//rmin - минимальное расстояние между точками поворота
	double rmax;//rmax - максимально - допустимая длина ломаной
	double alf; //alf - максимальный угол поворота
};

//характеристики текущего варианта решения //TODO что является 0, а что 1?? true или false?
struct VariantParameters
{
	int isLoop; //Признак наличия петли //iloop
	int isSolutionExist; //Существует ли решение //iex
	int vertexCount; //количество вершин в решении //inmb
	int ireg; //количество чего-то TODO
	int isSmallR; //признак, есть ли отрезок, меньший rmin //ir
	double polylineLength; //rl
};

int fact1(int mmm, int xm[25], int ym[25])
{
	int i;
	System::Windows::Forms::PaintEventArgs^ e;
	for (i = 0;i<mmm;i++)
	{
		e->Graphics->DrawLine(System::Drawing::Pens::Green, xm[i], ym[i], xm[i + 1], ym[i + 1]);
	}
	return mmm + i;
}

int fact(int xm[25], int ym[25])
{
	// Для демонстрации
	const int nmbmax = 6; //максимальное число точек ломаной
	const int nmbp = 4; //число вершин многоугольника, задающего район
	double Pi = atan(1.) * 4;
	Polyline *z; // Координаты точек, результирующей ломанную
	Polyline p; //координаты вершин многоугольника, задающего район
	
	Parameters parameters;
	parameters.alf = Pi / 3; // максимальный угол поворота
	parameters.rmax = 100; //максимально-допустимая длина ломаной
	parameters.rmin = 1; //минимальное расстояние между вершинами
	parameters.initialVec = { { 4.5, 5.0 },{ -1, 0 } };
	parameters.finalVec = { { 2,2 } ,{ 0.8, -0.6 } };


	Point2D tempP[nmbp] = { {0,0},{10,0},{10,10},{0,10} }; //содаем массив вершин ,ограничивающий некоторую область
	p = { nmbp, tempP }; //инициализируем эту область

	//вычисляем ломанную, удовлтворяющую требованиям
	z = EndSelect(parameters, p);

	for (int k = 0;k < z->vertex_count; k++)
	{
		xm[k] = floor(z->vertex[k].x * 150);
		ym[k] = floor(900 - z->vertex[k].y * 150);
	}
	return z->vertex_count;
}

/*
Функция предназначена для определения лучшего варианта построения касательных для определения точек
поворота между началом входного и концом конечного векторов при условии:
 - угол поворота < alf;
 - длина отрезка между точками поворота > rmin.
Результат -число  точек поворота в лучшем варианте началом входного и концом конечного векторов

Формальные  аргументы.
 При входе :
 parameters - параметры для раьоты алгоритма
 nmbmax -максимально-допустимое число точек ломаной
 p.vertex[i] -координаты вершин многоугольника, задающего район
 nmbp -число вершин многоугольника, задающего район
 При выходе
	zxy.vertex[i] - координаты точек поворота лучшего варианта
	(точки нумеруются начиная с начала ломаной до ее конца).
    zxy.vertex[0] - координаты начала входного вектора
    ...
    zxy.vertex[nmb-1]-координаты конца конечного вектора
 где zxy.vertex_cont - общее число точек ломаной, включая точки начала и конца.
 */
//TODO
//VariantParameters.polylineLength (rl) нигде не используется, не присваивается и не меняется, в других функциях не возвращается
//?выбираем вариант построения маршрута?
Polyline* EndSelect(Parameters parameters, Polyline &p)
{
	const int maxVertexCount = 25;
   int i,k,indreg,ipar[4];
   int choosenVariant; //Выбранный вариант построения

   VariantParameters currentVarParameters;
   VariantParameters varParameters[5];

   Polyline* zxy;

   //Переберем все различные варианты построения маршрута
   for (i=1;i<5;i++)
   {
       choosenVariant=i; //Выбираем i-Ый вариант

	   //построим по i-ому варианту ломаную
	   zxy = EndMarsh(parameters, currentVarParameters.polylineLength, choosenVariant, ipar);
	   //извлекаем из ipar параметры
	   currentVarParameters.isSolutionExist = ipar[0];
	   currentVarParameters.vertexCount = ipar[1];
	   currentVarParameters.isLoop = ipar[2];
	   currentVarParameters.isSmallR = ipar[3];
	   currentVarParameters.ireg = RegBoundary(*zxy, p, indreg);

	   //сохраняем параметры для текущего рассмастриваемого случая
	   varParameters[i] = currentVarParameters;
  }

   // выбор допустимых вариантов по nmb с учетом inds,indreg,indr
  for (i=1;i<5;i++)
  {
      if(varParameters[i].isSolutionExist ==0) //если решение существует
      {
          if(varParameters[i].ireg >0) 
			  varParameters[i].isSolutionExist = 0; // то решение существует
          if(varParameters[i].isSmallR > 0)  
			  varParameters[i].isSolutionExist = 1; //если есть отрезок, длины меньшей R, то решение отсутствует
      }
  }

   // наилучший путь по nmb, при этом учет: indloop, rlen
   currentVarParameters.vertexCount = maxVertexCount;
   choosenVariant = -1; //текущий выбранный вариант решения
   for (i=1;i<5;i++)
     {
	    if(varParameters[i].isSolutionExist == 0) //Если решение существует
	    {	
            if(varParameters[i].vertexCount < currentVarParameters.vertexCount) //если количство вершин меньше максимального
		    {
				currentVarParameters.vertexCount = varParameters[i].vertexCount; //выставим новое максимальное значение верщин
                choosenVariant = i; //выбранный вариант решения теперь i
		    }
        }
     } 
   if(choosenVariant > 0) //если выбрали решение (оно существует)
   {
	   //Сохраняем параметры решения под номером knmb
       currentVarParameters.isLoop = varParameters[choosenVariant].isLoop; 
       currentVarParameters.polylineLength = varParameters[choosenVariant].polylineLength;
   }

   choosenVariant = choosenVariant; // Выбранный вариант теперь knmb

   //еще раз выполняем построение ломаной, но уже для оптимального случая
   zxy = EndMarsh(parameters, currentVarParameters.polylineLength, choosenVariant, ipar);
   currentVarParameters.vertexCount=ipar[1];
   zxy->vertex_count = currentVarParameters.vertexCount;

   return zxy;
}

/*
Функция предназначена для определения точек поворота между началом входного и концом конечного векторов при условии:
 - угол поворота < alf;
 - длина отрезка между точками поворота > rmin.
 Результат -число  точек поворота между началом входного и концом конечного векторов

Формальные аргументы.
 При входе :
  parameters - параметры для работы алгоритма
 choosenVariant - номер варианта касательных
 zxy.vertex_count -максимально-допустимое число точек ломаной
 
 При выходе
	zxy.vertex[i] - координаты точки поворота с номером i
  (точки нумеруются начиная с начала ломаной до ее конца).
 При этом :
   zxy.vertex[0]  - координаты начала входного вектора
   ...
   zxy.vertex[nmb-1] -координаты конца конечного вектора
 где zxy.vertex_count - общее число точек ломаной, включая точки начала и конца.
 rlen - длина ломаной
 ipar -
 ipar[0] - признак существования решения
 ipar[1] - общее число точек ломаной
 ipar[2] - признак наличия петли
 ipar[3] - признак наличия отрезка с длиной меньшей,чем rmin
 */
//TODO ipar поменять на VariantParameters
//Строим маршрут
Polyline* EndMarsh(Parameters parameters,double rlen, int indvar,int* ipar)
{
   Point2D kn, kf, on, of;
   Vector n, f;
   double r,rr,d,d1,fin,fout,Pi,beta,gamm;
   int inds,nmb,indloop,indr;
   int nmb1,nmb2,i;
   Pi=atan(1.)*4;

   Polyline zxy;

   n.begin = parameters.initialVec.begin;

   // конец входного вектора 
   fin=myAngleP(parameters.initialVec.end);
   n.end = { n.begin.x + parameters.rmin*cos(fin), n.begin.y + parameters.rmin*sin(fin) };

   //начало финального вектора
   f.end = parameters.finalVec.end;

   fout=myAngleP(parameters.finalVec.end);

   f.begin = { f.end.x - parameters.rmin*cos(fout), f.end.y - parameters.rmin*sin(fout) };

   //Инициализируем и заполняем массив нулями
   zxy.vertex_count = 25;
   zxy.vertex = new Point2D[zxy.vertex_count];
   for (i=0;i<zxy.vertex_count;i++)
   {
	   zxy.vertex[i] = { 0, 0 };
   }

   //определение центров окужностей
   r= parameters.rmin/(2*sin(parameters.alf/2));
   if((indvar==1)||(indvar==2))
   {
	  gamm=fin+Pi*0.5+ parameters.alf/2;
   }
   else
   {
	  gamm=fin-Pi*0.5- parameters.alf/2;
   }

   on = { n.end.x + r*cos(gamm), n.end.y + r*sin(gamm) };

   if((indvar==1)||(indvar==3))
   {
	  gamm=fin+Pi*0.5+ parameters.alf/2;
   }
   else
   {
	  gamm=fin-Pi*0.5- parameters.alf/2;
   }

   of = { f.end.x + r*cos(gamm), f.end.y + r*sin(gamm) };


   //определение существования решений
   if((indvar==1)||(indvar==4))
   {
      inds=0;
   }
   else
   {
	  d=myMod(on, of);
	  if((d-2*r)<0) inds=1;
	  else inds=0;
   }

   //определение точек касания
   if(inds==0)
   {
      gamm=myAngle(of, on);
      if(indvar==1)
	  {
         d=-Pi*0.5+gamm;
	     d1=d;
      }
      if(indvar==4)
	  {
         d=Pi*0.5+gamm;
	     d1=d;
      }
      if(indvar==2)
	  {
         rr=sqrt((of.x-on.x)*(of.x-on.x)+(of.y-on.y)*(of.y-on.y));
	     beta=asin((2*r)/rr);
         d=gamm+beta-Pi*0.5;
	     d1=gamm+beta+Pi*0.5;
      }
	  if (indvar == 1)
	  {
		  rr = sqrt((of.x - on.x)*(of.x - on.x) + (of.y - on.y)*(of.y - on.y));
		  beta = asin((2 * r) / rr);
		  d = gamm + beta + Pi*0.5;
		  d1 = gamm + beta - Pi*0.5;
	  }

	  kn = { on.x + r*cos(d), on.y + r*sin(d) };
	  kf = { of.x + r*cos(d1), of.y + r*sin(d1) };

      //определение точек поворота  
      gamm=myArg(kn,kf);
      if((indvar==1)||(indvar==2))
      {
	      d=gamm-fin;
	      d1= parameters.alf;
      }
      else
      {
	      d=fin-gamm;
 	      d1=-parameters.alf;
      }
      if(d>2*Pi) d=d-2*Pi;
      if(d<0) d=d+2*Pi;
      nmb1=d/ parameters.alf;
	zxy.vertex[0] = n.begin;
	zxy.vertex[1] = n.end;

      if(nmb1>0)
      {
          for (i=1;i<nmb1+1;i++)
          {
              d=i*d1+fin;
			  zxy.vertex[i + 1] = { zxy.vertex[i].x + parameters.rmin*cos(d), zxy.vertex[i].y + parameters.rmin*sin(d) };
          }
      }
      if((indvar==1)||(indvar==3))
      {
	      d=fout-gamm;
	      d1= parameters.alf;
      }
      else
      {
	      d=gamm-fout;
	      d1=-parameters.alf;
      }
      if(d>2*Pi) d=d-2*Pi;
      if(d<0) d=d+2*Pi;
      nmb2=d/ parameters.alf;
      zxy.vertex[nmb1+nmb2+2]=f.begin;
      zxy.vertex[nmb1+nmb2+3]=f.end;
      if(nmb2>0)
      {
          for (i=1;i<nmb2+1;i++)
          {
              d=-i*d1+fout-Pi;
			  zxy.vertex[nmb1 + nmb2 + 2 - i] = { zxy.vertex[nmb1 + nmb2 + 3 - i].x + parameters.rmin*cos(d), zxy.vertex[nmb1 + nmb2 + 3 - i].y + parameters.rmin*sin(d) };
          }
      }
//
      nmb=nmb1+nmb2+4;
      d=myMod(zxy.vertex[nmb1+1],zxy.vertex[nmb1+2]);
      if(d<parameters.rmin)
      {
	      indr=1;
      }
      if((nmb1+1)>(2*Pi/ parameters.alf))
      {
	      indloop=1;
      }
      if((nmb2+1)>(2*Pi/ parameters.alf))
      {
	      indloop=1;
      }
      rlen=0;
      for (i=1;i<nmb;i++)
      {
          d=zxy.vertex[i].y-zxy.vertex[i-1].y;
          d1=zxy.vertex[i].x-zxy.vertex[i-1].x;
          rr=sqrt(d*d+d1*d1);
          rlen=rlen+rr;
      }
//==================
      ipar[0]=inds;
      ipar[1]=nmb;
      ipar[2]=indloop;
      ipar[3]=indr;
   }
   return &zxy;
}


//Количесво ????
int RegBoundary(Polyline &zxy, Polyline &p,int indreg)
{
   int i,k;
   double d1,d2,Pi;
   Pi=atan(1.)*4;
   indreg=0;
   for (i=0;i<zxy.vertex_count;i++)
   {
      for (k=0;i<p.vertex_count-1;i++)
      {
         d1=myArg(zxy.vertex[i],p.vertex[k]);
         d2=myArg(zxy.vertex[i],p.vertex[k+1]);
         d2=d2-d1;
         if(d2<0) d2=d2+2*Pi;  //делаем так, чтобы угол был положительый
         if(d2>=Pi) indreg=indreg+1; //если угол больше, чем Pi, то indreg++
      }
	  //учитываем нерасмотренные случаи
     d1=myArg(zxy.vertex[i],p.vertex[p.vertex_count - 1]); //угол между Ni и Pn-1
     d2=myArg(zxy.vertex[i],p.vertex[0]); //угол между Ni и P0
     d2=d2-d1;
     if(d2<0)d2=d2+2*Pi;
     if(d2>=Pi)indreg=indreg+1;
   }
   return indreg;
}
//вычисляем угол поворота между двумя точками
double myArg(Point2D &n1,Point2D &f1)
{
   double  d,dd,ddd,pi;
   pi=atan(1.)*4;
   d=f1.y-n1.y;
   dd=f1.x-n1.x;
   ddd=sqrt(d*d+dd*dd); //Расстояние между двумя точками
   d=d/ddd; //cos
   dd=dd/ddd; //sin
   if((d>=0)&&(dd>=0)) ddd=asin(d);
   if((d>=0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
   return ddd;
}


//модуль вектора (расстояние между двумя точками) (nx,ny)-начало,(fx,fy)-конец)
double myMod(Point2D &n1, Point2D &f1)
{
   double d,dd,ddd;
   d=f1.y-n1.y;
   dd=f1.x-n1.x;
   ddd=sqrt(d*d+dd*dd);
   return ddd;
}

double myAngle(Point2D &n1, Point2D &f1)
{
   double d,dd,ddd,pi;
   pi=atan(1.)*4;
   d=n1.y-f1.y;
   dd=n1.x-f1.x;
   ddd=sqrt(d*d+dd*dd);
   d=d/ddd;
   dd=dd/ddd;
   if((d>=0)&&(dd>=0)) ddd=asin(d);
   if((d>=0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
   return ddd;
}

double myAngleP(Point2D &n1)
{
   double d,dd,ddd,pi;
   pi=atan(1.)*4;
   d=n1.y;
   dd=n1.x;
   ddd=sqrt(d*d+dd*dd);
   d=d/ddd;
   dd=dd/ddd;
   if((d>=0)&&(dd>=0)) ddd=asin(d);
   if((d>=0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd<0)) ddd=pi-asin(d);
   if((d<0)&&(dd>=0)) ddd=2*pi+asin(d);
   return ddd;
}