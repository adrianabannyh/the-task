#pragma onces
#include "math.h"
#include "MyForm.h"
#include"fact.h"



int fact1(int vertexCount, int xm[25], int ym[25], System::Windows::Forms::PaintEventArgs^ e)
{
	int i;
	for (int i = 0;i<vertexCount;i++)
	{
		e->Graphics->DrawLine(System::Drawing::Pens::Green, xm[i], ym[i], xm[i + 1], ym[i + 1]);
	}
	return vertexCount + i;
}

int fact(int xm[25], int ym[25])
{
	// ��� ������������
	const int nmbmax = 6; //������������ ����� ����� �������
	const int nmbp = 4; //����� ������ ��������������, ��������� �����
	double Pi = atan(1.) * 4;
	Polyline *z; // ���������� �����, �������������� ��������
	Polyline p; //���������� ������ ��������������, ��������� �����

	Parameters parameters;
	parameters.alf = Pi / 3; // ������������ ���� ��������
	parameters.rmax = 100; //�����������-���������� ����� �������
	parameters.rmin = 1; //����������� ���������� ����� ���������
	parameters.initialVec = { { 4.5, 5.0 },{ -1, 0 } };
	parameters.finalVec = { { 2,2 } ,{ 0.8, -0.6 } };


	Point2D tempP[nmbp] = { { 0,0 },{ 10,0 },{ 10,10 },{ 0,10 } }; //������ ������ ������ ,�������������� ��������� �������
	p = { nmbp, tempP }; //�������������� ��� �������

						 //��������� ��������, �������������� �����������
	z = EndSelect(parameters, p);

	for (int k = 0;k < z->vertex_count; k++)
	{
		xm[k] = floor(10+z->vertex[k].x * 60);
		ym[k] = floor(400 - z->vertex[k].y * 60);
	}
	return z->vertex_count;
}

/*
������� ������������� ��� ����������� ������� �������� ���������� ����������� ��� ����������� �����
�������� ����� ������� �������� � ������ ��������� �������� ��� �������:
- ���� �������� < alf;
- ����� ������� ����� ������� �������� > rmin.
��������� -�����  ����� �������� � ������ �������� ������� �������� � ������ ��������� ��������

����������  ���������.
��� ����� :
parameters - ��������� ��� ������ ���������
nmbmax -�����������-���������� ����� ����� �������
p.vertex[i] -���������� ������ ��������������, ��������� �����
nmbp -����� ������ ��������������, ��������� �����
��� ������
zxy.vertex[i] - ���������� ����� �������� ������� ��������
(����� ���������� ������� � ������ ������� �� �� �����).
zxy.vertex[0] - ���������� ������ �������� �������
...
zxy.vertex[nmb-1]-���������� ����� ��������� �������
��� zxy.vertex_cont - ����� ����� ����� �������, ������� ����� ������ � �����.
*/
//TODO
//VariantParameters.polylineLength (rl) ����� �� ������������, �� ������������� � �� ��������, � ������ �������� �� ������������
//?�������� ������� ���������� ��������?
Polyline* EndSelect(Parameters parameters, Polyline &p)
{
	const int maxVertexCount = 25;
	int i, k, indreg, ipar[4];
	int choosenVariant; //��������� ������� ����������

	VariantParameters currentVarParameters;
	VariantParameters varParameters[5];

	Polyline* zxy;

	//��������� ��� ��������� �������� ���������� ��������
	for (i = 1;i<5;i++)
	{
		choosenVariant = i; //�������� i-�� �������

							//�������� �� i-��� �������� �������
		zxy = EndMarsh(parameters, currentVarParameters.polylineLength, choosenVariant, ipar);
		//��������� �� ipar ���������
		currentVarParameters.isSolutionExist = ipar[0];
		currentVarParameters.vertexCount = ipar[1];
		currentVarParameters.isLoop = ipar[2];
		currentVarParameters.isSmallR = ipar[3];
		currentVarParameters.ireg = RegBoundary(*zxy, p, indreg);

		//��������� ��������� ��� �������� ����������������� ������
		varParameters[i] = currentVarParameters;
	}

	// ����� ���������� ��������� �� nmb � ������ inds,indreg,indr
	for (i = 1;i<5;i++)
	{
		if (varParameters[i].isSolutionExist == 0) //���� ������� ����������
		{
			if (varParameters[i].ireg >0)
				varParameters[i].isSolutionExist = 0; // �� ������� ����������
			if (varParameters[i].isSmallR > 0)
				varParameters[i].isSolutionExist = 1; //���� ���� �������, ����� ������� R, �� ������� �����������
		}
	}

	// ��������� ���� �� nmb, ��� ���� ����: indloop, rlen
	currentVarParameters.vertexCount = maxVertexCount;
	choosenVariant = -1; //������� ��������� ������� �������
	for (i = 1;i<5;i++)
	{
		if (varParameters[i].isSolutionExist == 0) //���� ������� ����������
		{
			if (varParameters[i].vertexCount < currentVarParameters.vertexCount) //���� ��������� ������ ������ �������������
			{
				currentVarParameters.vertexCount = varParameters[i].vertexCount; //�������� ����� ������������ �������� ������
				choosenVariant = i; //��������� ������� ������� ������ i
			}
		}
	}
	if (choosenVariant > 0) //���� ������� ������� (��� ����������)
	{
		//��������� ��������� ������� ��� ������� knmb
		currentVarParameters.isLoop = varParameters[choosenVariant].isLoop;
		currentVarParameters.polylineLength = varParameters[choosenVariant].polylineLength;
	}

	choosenVariant = choosenVariant; // ��������� ������� ������ knmb

									 //��� ��� ��������� ���������� �������, �� ��� ��� ������������ ������
	zxy = EndMarsh(parameters, currentVarParameters.polylineLength, choosenVariant, ipar);
	currentVarParameters.vertexCount = ipar[1];
	zxy->vertex_count = currentVarParameters.vertexCount;

	return zxy;
}

/*
������� ������������� ��� ����������� ����� �������� ����� ������� �������� � ������ ��������� �������� ��� �������:
- ���� �������� < alf;
- ����� ������� ����� ������� �������� > rmin.
��������� -�����  ����� �������� ����� ������� �������� � ������ ��������� ��������

���������� ���������.
��� ����� :
parameters - ��������� ��� ������ ���������
choosenVariant - ����� �������� �����������
zxy.vertex_count -�����������-���������� ����� ����� �������

��� ������
zxy.vertex[i] - ���������� ����� �������� � ������� i
(����� ���������� ������� � ������ ������� �� �� �����).
��� ���� :
zxy.vertex[0]  - ���������� ������ �������� �������
...
zxy.vertex[nmb-1] -���������� ����� ��������� �������
��� zxy.vertex_count - ����� ����� ����� �������, ������� ����� ������ � �����.
rlen - ����� �������
ipar -
ipar[0] - ������� ������������� �������
ipar[1] - ����� ����� ����� �������
ipar[2] - ������� ������� �����
ipar[3] - ������� ������� ������� � ������ �������,��� rmin
*/
//TODO ipar �������� �� VariantParameters
//������ �������
Polyline* EndMarsh(Parameters parameters, double rlen, int indvar, int* ipar)
{
	Point2D kn, kf, on, of;
	Vector n, f;
	double r, rr, d, d1, fin, fout, Pi, beta, gamm;
	int inds, nmb, indloop, indr;
	int nmb1, nmb2, i;
	Pi = atan(1.) * 4;

	Polyline* zxy = new Polyline();

	n.begin = parameters.initialVec.begin;

	// ����� �������� ������� 
	fin = myAngleP(parameters.initialVec.end);
	n.end = { n.begin.x + parameters.rmin*cos(fin), n.begin.y + parameters.rmin*sin(fin) };

	//������ ���������� �������
	f.end = parameters.finalVec.end;

	fout = myAngleP(parameters.finalVec.end);

	f.begin = { f.end.x - parameters.rmin*cos(fout), f.end.y - parameters.rmin*sin(fout) };

	//�������������� � ��������� ������ ������
	zxy->vertex_count = 25;
	zxy->vertex = new Point2D[zxy->vertex_count];
	for (i = 0;i<zxy->vertex_count;i++)
	{
		zxy->vertex[i] = { 0, 0 };
	}

	//����������� ������� ����������
	r = parameters.rmin / (2 * sin(parameters.alf / 2));
	if ((indvar == 1) || (indvar == 2))
	{
		gamm = fin + Pi*0.5 + parameters.alf / 2;
	}
	else
	{
		gamm = fin - Pi*0.5 - parameters.alf / 2;
	}

	on = { n.end.x + r*cos(gamm), n.end.y + r*sin(gamm) };

	if ((indvar == 1) || (indvar == 3))
	{
		gamm = fin + Pi*0.5 + parameters.alf / 2;
	}
	else
	{
		gamm = fin - Pi*0.5 - parameters.alf / 2;
	}

	of = { f.end.x + r*cos(gamm), f.end.y + r*sin(gamm) };


	//����������� ������������� �������
	if ((indvar == 1) || (indvar == 4))
	{
		inds = 0;
	}
	else
	{
		d = myMod(on, of);
		if ((d - 2 * r)<0) inds = 1;
		else inds = 0;
	}

	//����������� ����� �������
	if (inds == 0)
	{
		gamm = myAngle(of, on);
		if (indvar == 1)
		{
			d = -Pi*0.5 + gamm;
			d1 = d;
		}
		if (indvar == 4)
		{
			d = Pi*0.5 + gamm;
			d1 = d;
		}
		if (indvar == 2)
		{
			rr = sqrt((of.x - on.x)*(of.x - on.x) + (of.y - on.y)*(of.y - on.y));
			beta = asin((2 * r) / rr);
			d = gamm + beta - Pi*0.5;
			d1 = gamm + beta + Pi*0.5;
		}
		if (indvar == 1)
		{
			rr = sqrt((of.x - on.x)*(of.x - on.x) + (of.y - on.y)*(of.y - on.y));
			beta = asin((2 * r) / rr);
			d = gamm + beta + Pi*0.5;
			d1 = gamm + beta - Pi*0.5;
		}

		kn = { on.x + r*cos(d), on.y + r*sin(d) };
		kf = { of.x + r*cos(d1), of.y + r*sin(d1) };

		//����������� ����� ��������  
		gamm = myArg(kn, kf);
		if ((indvar == 1) || (indvar == 2))
		{
			d = gamm - fin;
			d1 = parameters.alf;
		}
		else
		{
			d = fin - gamm;
			d1 = -parameters.alf;
		}
		if (d>2 * Pi) d = d - 2 * Pi;
		if (d<0) d = d + 2 * Pi;
		nmb1 = d / parameters.alf;
		zxy->vertex[0] = n.begin;
		zxy->vertex[1] = n.end;

		if (nmb1>0)
		{
			for (i = 1;i<nmb1 + 1;i++)
			{
				d = i*d1 + fin;
				zxy->vertex[i + 1] = { zxy->vertex[i].x + parameters.rmin*cos(d), zxy->vertex[i].y + parameters.rmin*sin(d) };
			}
		}
		if ((indvar == 1) || (indvar == 3))
		{
			d = fout - gamm;
			d1 = parameters.alf;
		}
		else
		{
			d = gamm - fout;
			d1 = -parameters.alf;
		}
		if (d>2 * Pi) d = d - 2 * Pi;
		if (d<0) d = d + 2 * Pi;
		nmb2 = d / parameters.alf;
		zxy->vertex[nmb1 + nmb2 + 2] = f.begin;
		zxy->vertex[nmb1 + nmb2 + 3] = f.end;
		if (nmb2>0)
		{
			for (i = 1;i<nmb2 + 1;i++)
			{
				d = -i*d1 + fout - Pi;
				zxy->vertex[nmb1 + nmb2 + 2 - i] = { zxy->vertex[nmb1 + nmb2 + 3 - i].x + parameters.rmin*cos(d), zxy->vertex[nmb1 + nmb2 + 3 - i].y + parameters.rmin*sin(d) };
			}
		}
		//
		nmb = nmb1 + nmb2 + 4;
		d = myMod(zxy->vertex[nmb1 + 1], zxy->vertex[nmb1 + 2]);
		if (d<parameters.rmin)
		{
			indr = 1;
		}
		if ((nmb1 + 1)>(2 * Pi / parameters.alf))
		{
			indloop = 1;
		}
		if ((nmb2 + 1)>(2 * Pi / parameters.alf))
		{
			indloop = 1;
		}
		rlen = 0;
		for (i = 1;i<nmb;i++)
		{
			d = zxy->vertex[i].y - zxy->vertex[i - 1].y;
			d1 = zxy->vertex[i].x - zxy->vertex[i - 1].x;
			rr = sqrt(d*d + d1*d1);
			rlen = rlen + rr;
		}
		//==================
		ipar[0] = inds;
		ipar[1] = nmb;
		ipar[2] = indloop;
		ipar[3] = indr;
	}
	return zxy;
}


//��������� ????
int RegBoundary(Polyline &zxy, Polyline &p, int indreg)
{
	int i, k;
	double d1, d2, Pi;
	Pi = atan(1.) * 4;
	indreg = 0;
	for (i = 0;i<zxy.vertex_count;i++)
	{
		for (k = 0;i<p.vertex_count - 1;i++)
		{
			d1 = myArg(zxy.vertex[i], p.vertex[k]);
			d2 = myArg(zxy.vertex[i], p.vertex[k + 1]);
			d2 = d2 - d1;
			if (d2<0) d2 = d2 + 2 * Pi;  //������ ���, ����� ���� ��� ������������
			if (d2 >= Pi) indreg = indreg + 1; //���� ���� ������, ��� Pi, �� indreg++
		}
		//��������� �������������� ������
		d1 = myArg(zxy.vertex[i], p.vertex[p.vertex_count - 1]); //���� ����� Ni � Pn-1
		d2 = myArg(zxy.vertex[i], p.vertex[0]); //���� ����� Ni � P0
		d2 = d2 - d1;
		if (d2<0)d2 = d2 + 2 * Pi;
		if (d2 >= Pi)indreg = indreg + 1;
	}
	return indreg;
}
//��������� ���� �������� ����� ����� �������
double myArg(Point2D &n1, Point2D &f1)
{
	double  d, dd, ddd, pi;
	pi = atan(1.) * 4;
	d = f1.y - n1.y;
	dd = f1.x - n1.x;
	ddd = sqrt(d*d + dd*dd); //���������� ����� ����� �������
	d = d / ddd; //cos
	dd = dd / ddd; //sin
	if ((d >= 0) && (dd >= 0)) ddd = asin(d);
	if ((d >= 0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd >= 0)) ddd = 2 * pi + asin(d);
	return ddd;
}


//������ ������� (���������� ����� ����� �������) (nx,ny)-������,(fx,fy)-�����)
double myMod(Point2D &n1, Point2D &f1)
{
	double d, dd, ddd;
	d = f1.y - n1.y;
	dd = f1.x - n1.x;
	ddd = sqrt(d*d + dd*dd);
	return ddd;
}

double myAngle(Point2D &n1, Point2D &f1)
{
	double d, dd, ddd, pi;
	pi = atan(1.) * 4;
	d = n1.y - f1.y;
	dd = n1.x - f1.x;
	ddd = sqrt(d*d + dd*dd);
	d = d / ddd;
	dd = dd / ddd;
	if ((d >= 0) && (dd >= 0)) ddd = asin(d);
	if ((d >= 0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd >= 0)) ddd = 2 * pi + asin(d);
	return ddd;
}

double myAngleP(Point2D &n1)
{
	double d, dd, ddd, pi;
	pi = atan(1.) * 4;
	d = n1.y;
	dd = n1.x;
	ddd = sqrt(d*d + dd*dd);
	d = d / ddd;
	dd = dd / ddd;
	if ((d >= 0) && (dd >= 0)) ddd = asin(d);
	if ((d >= 0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd<0)) ddd = pi - asin(d);
	if ((d<0) && (dd >= 0)) ddd = 2 * pi + asin(d);
	return ddd;
}