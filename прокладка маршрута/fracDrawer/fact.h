#pragma once
// ����� �� ���������
struct Point2D
{
	double x;//���������� �� X
	double y;//���������� �� Y
};
// ������������� �� ��������� 
struct Polyline
{
	int vertex_count; //����� ������ � �������������� 
	Point2D* vertex; //������� ��������������, ��� ������ ������ 
};

struct Vector
{
	Point2D begin; //���������� ������ �������
	Point2D end; //���������������� ����������� �������
};

//��������� ��� ������ ���������
struct Parameters
{
	Vector initialVec; //��������� ������
	Vector finalVec;  //�������� ������
	double rmin;//rmin - ����������� ���������� ����� ������� ��������
	double rmax;//rmax - ����������� - ���������� ����� �������
	double alf; //alf - ������������ ���� ��������
};

//�������������� �������� �������� ������� //TODO ��� �������� 0, � ��� 1?? true ��� false?
struct VariantParameters
{
	int isLoop; //������� ������� ����� //iloop
	int isSolutionExist; //���������� �� ������� //iex
	int vertexCount; //���������� ������ � ������� //inmb
	int ireg; //���������� ����-�� TODO
	int isSmallR; //�������, ���� �� �������, ������� rmin //ir
	double polylineLength; //rl
};

int fact1(int mmm, int xm[25], int ym[25], System::Windows::Forms::PaintEventArgs^ e);
int fact(int xm[25], int ym[25]);
Polyline* EndSelect(Parameters parameters, Polyline &p);
Polyline* EndMarsh(Parameters parameters, double rlen, int indvar, int* ipar);
int RegBoundary(Polyline &zxy, Polyline &p, int indreg);
double myArg(Point2D &n1, Point2D &f1);
double myMod(Point2D &n1, Point2D &f1);
double myAngle(Point2D &n1, Point2D &f1);
double myAngleP(Point2D &n1);